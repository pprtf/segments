package com.noff.segments.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.stereotype.Component;
import com.noff.segments.model.SegmentsResponse;

@Component
@FeignClient(name = "get-segments", url = "${get-segments.url}")
public interface SegmentsClient {
    @GetMapping(value = "/get_segments")
    SegmentsResponse getSegments(@RequestParam("frame_id") String frameId,
                            @RequestParam("display_time") Long displayTime,
                            @RequestParam("uniq_req_id") String uniqReqId);

}