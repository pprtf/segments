package com.noff.segments;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Value;
import java.util.ArrayList;
import java.util.List;
import java.util.*;
import lombok.extern.slf4j.Slf4j;
import lombok.RequiredArgsConstructor;
import java.util.UUID;
import java.time.Instant;
import com.noff.segments.client.SegmentsClient;
import com.noff.segments.data.SegmentsRepository;
import com.noff.segments.data.SegmentsEntity;
import com.noff.segments.model.SegmentsResponse;
import lombok.experimental.FieldDefaults;
import lombok.AccessLevel;

@Component
@Slf4j
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SegmentsService implements InitializingBean{ //IB for afterPropertiesSet

	@Autowired 
	SegmentsClient gsClient;

	@Value("${segments.refsresh.timeout}") 
	Long REFRESH_TIMEOUT; 
	Thread segmentsRefresher = new Thread(this::doRefresh, "get-segments");

	@Value("${get-segments.url}")
	String segmentsUrl; 
	final SegmentsRepository gsRep;
	volatile List<SegmentsResponse> resps = new ArrayList<>(); 

	@Value("${displays.array}")
	String[] displays;

	private void doRefresh() {
		try {
			do {
				Thread.sleep(REFRESH_TIMEOUT);
				try {
					getSegments();
				} catch (Exception e) {
					log.warn("Get segments problemes");
					log.trace("GetSegments Exception", e);
				}
			} while (true);
		} catch (InterruptedException e){
			log.warn("Segments thread is interrupted");
			log.trace("GetSegments Thread", e);
			return;
		}
	}

	synchronized void getSegments()	{
		String uniqReqId = ""+(UUID.randomUUID().getMostSignificantBits() & Long.MAX_VALUE);
		int dispIndex = (int) (Math.random() * displays.length);
		String display = displays[dispIndex];
		Instant instant = Instant.now();
		// long time = instant.toEpochMilli();
		long time = instant.getEpochSecond();

		log.info("Getting segments from: {}",segmentsUrl);
		SegmentsResponse resp = gsClient.getSegments(display,time,uniqReqId);
		log.info("Get segments: {}", resp);
		gsRep.save(SegmentsEntity.toEntity(resp));
	}

	//запуск потока при старте приложения
	@Override
	public void afterPropertiesSet() throws Exception {
		Timer timer = new Timer();
		
		TimerTask delayedThreadStartTask = new TimerTask() {
			@Override public void run(){
				segmentsRefresher.start();
			} 
		};
		timer.schedule(delayedThreadStartTask, 5000); //задержка запуска потока при старте 
	}
}
