package com.noff.segments.data;
import java.util.*;
import lombok.*;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Column;
import java.time.LocalDateTime; 
import lombok.AllArgsConstructor;
import com.noff.segments.model.SegmentsResponse;

@Data
@Entity
@Table(name = "get_segments")
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class SegmentsEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String uniqReqId;
    String unitId;
    String displayTime;
    Integer totalReach;
    Float totalFloor;
    @Column(name = "total_floor_pd")
    Float totalFloorPD;
    @Column(name = "total_floor_lpd")
    Float totalFloorLPD;
    Integer macs;
    Integer ots;
    LocalDateTime dateRecord = LocalDateTime.now();

    public static SegmentsEntity toEntity(SegmentsResponse gsr){
        return new SegmentsEntity(null,
        gsr.getId(),
        gsr.getData().getUnitId(),
        gsr.getData().getDisplayTime(),
        gsr.getData().getAudience().getTotalReach(),
        gsr.getData().getAudience().getTotalFloor(),
        gsr.getData().getAudience().getTotalFloorPD(),
        gsr.getData().getAudience().getTotalFloorLPD(),
        gsr.getData().getAudience().getMacs(),
        gsr.getData().getAudience().getOts(),
        LocalDateTime.now());       
    }
}