package com.noff.segments.data;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import java.util.List;

@RepositoryRestResource
public interface SegmentsRepository extends CrudRepository<SegmentsEntity, Long> {
	// List<SegmentsEntity> findAll();
}