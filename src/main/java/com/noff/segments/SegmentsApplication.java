package com.noff.segments;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class SegmentsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SegmentsApplication.class, args);
	}

}
