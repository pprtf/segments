# Беру два имеджа, т.к. нужно собрать на чем-то, где есть JVM (JDK),
# запускаю уже на jre-slim. Если использовать просто openjdk:11-slim,
# то образ будет весить больше.


FROM maven:3.6.0-jdk-11-slim AS TEMP_BUILD_IMAGE
RUN mkdir -p /home/app
WORKDIR /home/app/
COPY . .
RUN mvn clean package -DskipTests=true


FROM openjdk:11-jre-slim
ENV ARTIFACT_NAME=segments-0.0.1-SNAPSHOT.jar
RUN mkdir -p /home/app
WORKDIR /home/app/
COPY --from=TEMP_BUILD_IMAGE /home/app/target/$ARTIFACT_NAME .

EXPOSE 8080
# ENTRYPOINT ["java", "-jar", "-Dspring.profiles.active=docker", "./segments-0.0.1-SNAPSHOT.jar"]
ENTRYPOINT ["java", "-jar", "./segments-0.0.1-SNAPSHOT.jar"]